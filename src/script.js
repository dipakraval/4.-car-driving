import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'dat.gui'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'

import CANNON from 'cannon'

/**
 * Texture
 */
 const ground = new THREE.TextureLoader().load( '/modal/texture/1.jpg' );
 const sky = new THREE.TextureLoader().load( '/modal/texture/sky.jpg' );

// Debug
const gui = new dat.GUI()

// Canvas
const canvas = document.querySelector('canvas.webgl')

const path =
      "https://s3-us-west-2.amazonaws.com/s.cdpn.io/1147877/winter-hdri_";
const format = ".png";
const order = ["px", "nx", "py", "ny", "pz", "nz"];
const urls = [];

order.forEach(side => {
    urls.push(`${path}${side}${format}`);
});
const textureCube = new THREE.CubeTextureLoader().load(urls);
textureCube.format = THREE.RGBFormat;



// Scene
const scene = new THREE.Scene()
scene.background = textureCube;
// scene.background = new THREE.Color(0x000000);

/**
 * Camera
 */
const size = {
    width: window.innerWidth,
    height: window.innerHeight
}
const camera = new THREE.PerspectiveCamera(75,size.width / size.height,0.1,1000);
camera.position.set(0, 2.4, 9.0)

scene.add( camera );



/**
 * GeoMetriy
 */

var geometry = new THREE.PlaneGeometry(50, 50, 50);
var material = new THREE.MeshPhongMaterial({
    map: ground,
    side: THREE.DoubleSide
});
var plane = new THREE.Mesh(geometry, material);
plane.rotation.x = Math.PI/2;
scene.add(plane);



/**
 * Light
 */
var sunlight = new THREE.DirectionalLight(0xffffff, 1.0);
sunlight.position.set(-10, 10, 0);
scene.add(sunlight)


/**
* Physics
**/
var world = new CANNON.World();
world.broadphase = new CANNON.SAPBroadphase(world);
world.gravity.set(0, -10, 0);
world.solver.iterations = 10
world.defaultContactMaterial.friction = 0;

var groundMaterial = new CANNON.Material('groundMaterial');
var wheelMaterial = new CANNON.Material('wheelMaterial');
var wheelGroundContactMaterial = new CANNON.ContactMaterial(wheelMaterial, groundMaterial, {
    friction: 0.3,
    restitution: 0,
    contactEquationStiffness: 1000,
});
world.addContactMaterial(wheelGroundContactMaterial);


// car physics body
var chassisShape = new CANNON.Box(new CANNON.Vec3(1, 0.3, 2));
var chassisBody = new CANNON.Body({mass: 150});
chassisBody.addShape(chassisShape);
chassisBody.position.set(0, 0.2, 0);

chassisBody.angularVelocity.set(0, 0, 0); // initial velocity

// car visual body
let model = new THREE.Object3D();

const loader = new GLTFLoader();


loader.load( '/modal/Low-Poly-Racing-Car.gltf', function ( gltf ) {
    model = gltf.scene;
    model.rotation.set(0,9.5,0);
    scene.add(model);
});




// parent vehicle object
var vehicle = new CANNON.RaycastVehicle({
    chassisBody: chassisBody,
    indexRightAxis: 0, // x
    indexUpAxis: 1, // y
    indexForwardAxis: 2, // z
});


// wheel options
var options = {
    radius: 0.3,
    directionLocal: new CANNON.Vec3(0, -1, 0),
    suspensionStiffness: 45,
    suspensionRestLength: 0.4,
    frictionSlip: 5,
    dampingRelaxation: 2.3,
    dampingCompression: 4.5,
    maxSuspensionForce: 300,
    minSuspensionForce: 300,
    rollInfluence:  0.01,
    axleLocal: new CANNON.Vec3(-1, 0, 0),
    chassisConnectionPointLocal: new CANNON.Vec3(1, 1, 0),
    maxSuspensionTravel: 0.25,
    customSlidingRotationalSpeed: -30,
    useCustomSlidingRotationalSpeed: true,
};


var axlewidth = 0.65;
options.chassisConnectionPointLocal.set(axlewidth, 0, -0.84);
vehicle.addWheel(options);

options.chassisConnectionPointLocal.set(-axlewidth, 0, -0.84);
vehicle.addWheel(options);

options.chassisConnectionPointLocal.set(axlewidth, 0, 0.42);
vehicle.addWheel(options);

options.chassisConnectionPointLocal.set(-axlewidth, 0, 0.42);
vehicle.addWheel(options);

vehicle.addToWorld(world);


// car wheels
var wheelBodies = [],
    wheelVisuals = [];
vehicle.wheelInfos.forEach(function(wheel) {
    var shape = new CANNON.Cylinder(0.2, 0.2, 0.2, 10);
    var body = new CANNON.Body({mass: 1, material: wheelMaterial});
    var q = new CANNON.Quaternion();
    q.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), Math.PI / 2);
    body.addShape(shape, new CANNON.Vec3(), q);
    wheelBodies.push(body);
    // wheel visual body
    var geometry = new THREE.CylinderGeometry( 0.15, 0.15, 0.09, 50);
    var material = new THREE.MeshPhongMaterial({
        color: 0x808080,
        side: THREE.DoubleSide,
        flatShading: true,
    });
    var cylinder = new THREE.Mesh(geometry, material);
    cylinder.geometry.rotateZ(Math.PI/2);
    wheelVisuals.push(cylinder);
    scene.add(cylinder);
});

// update the wheels to match the physics
world.addEventListener('postStep', function() {
    for (var i=0; i<vehicle.wheelInfos.length; i++) {
      vehicle.updateWheelTransform(i);
      var t = vehicle.wheelInfos[i].worldTransform;
      // update wheel physics
      wheelBodies[i].position.copy(t.position);
      wheelBodies[i].quaternion.copy(t.quaternion);
      // update wheel visuals
      wheelVisuals[i].position.copy(t.position);
      wheelVisuals[i].quaternion.copy(t.quaternion);
    }
});

var q = plane.quaternion;
var planeBody = new CANNON.Body({
    mass: 0, // mass = 0 makes the body static
    material: groundMaterial,
    shape: new CANNON.Plane(),
    quaternion: new CANNON.Quaternion(-q._x, q._y, q._z, q._w)
});
world.add(planeBody)


/**
* Main
**/

function updatePhysics() {
    world.step(1/60);
    // update the chassis position
    model.position.copy(chassisBody.position);
    model.quaternion.copy(chassisBody.quaternion);
}


function render() {
    requestAnimationFrame(render);
    renderer.render(scene, camera);
    updatePhysics();
}

function navigate(e) {
    if (e.type != 'keydown' && e.type != 'keyup') return;
    var keyup = e.type == 'keyup';
    vehicle.setBrake(0, 0);
    vehicle.setBrake(0, 1);
    vehicle.setBrake(0, 2);
    vehicle.setBrake(0, 3);
  
    var engineForce = 800,
        maxSteerVal = 0.3;

    switch(e.keyCode) {
  
      case 38: // forward
        vehicle.applyEngineForce(keyup ? 0 : -engineForce, 2);
        vehicle.applyEngineForce(keyup ? 0 : -engineForce, 3);
        break;
  
      case 40: // backward
        vehicle.applyEngineForce(keyup ? 0 : engineForce, 2);
        vehicle.applyEngineForce(keyup ? 0 : engineForce, 3);
        break;
  
      case 39: // right
        vehicle.setSteeringValue(keyup ? 0 : -maxSteerVal, 2);
        vehicle.setSteeringValue(keyup ? 0 : -maxSteerVal, 3);
        break;
  
      case 37: // left
        vehicle.setSteeringValue(keyup ? 0 : maxSteerVal, 2);
        vehicle.setSteeringValue(keyup ? 0 : maxSteerVal, 3);
        break;

    }
}

/**
 * OrbitControls
*/ 
const control = new OrbitControls(camera, canvas);
control.enableZoom = true;
control.enableDamping = true;

control.minDistance = 4;
control.maxDistance = 28;
control.minPolarAngle = Math.PI/5; // radians
control.maxPolarAngle = Math.PI/2.2; 


/**
 * Renderer
*/
const renderer = new THREE.WebGLRenderer({
    canvas: canvas,
    alpha: true
})
renderer.setSize(size.width,size.height);

window.addEventListener('keydown', navigate)
window.addEventListener('keyup', navigate)

render();

/**
 * Animation
 */
const clock = new THREE.Clock()
const tick = () => {

    control.update();
    renderer.render(scene,camera);
    window.requestAnimationFrame(tick);

}
tick();